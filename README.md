# Python IDE for ARM Processors based on [Micropython](https://github.com/micropython/micropython) framework. Here is the [Demo.](https://pyinarm.herokuapp.com/)

[![Electron](https://img.shields.io/badge/IDE-Python-brightgreen.svg) ](https://github.com/arturgoms/PyinArm) [ ![Gitter](https://img.shields.io/badge/release-82%25-green.svg) ](https://github.com/arturgoms/PyinArm/wiki) [![](https://img.shields.io/badge/python-2.7-blue.svg)](https://www.python.org/download/releases/2.7/) [![](https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&style=flat)](https://www.paypal.me/t) 

![SDVersion](https://dl.dropboxusercontent.com/s/za2scs6mfhyf5xe/pyrmGrid.png?dl=0)



## How it works

It is an IDE created in Flask and based on the microPython framwork, everything was done in html and css, some parts in javascript and it was also used the library Codemirror for the syntax in the python editor.

### Support ( for now )

	Teensy 3.1
    Teensy 3.5
    Teensy 3.6
    
### Credits
	Micropython Framework
	Codemirror for editor 
	Semantic UI
	DIBBLED for the logo 
 
 Logo is under creativecommons.org/licenses/by/4.0/ license <br>
 @DIBBLED <a href="https://forum.xda-developers.com/member.php?u=8283384">XDA</a> & <a href="https://plus.google.com/u/1/113925592652668928530">G+</a> <br>
Contact: dibbledesigns14@gmail.com
 
### Manual installation


 First you need to clone the directory:
 ```bash
      git clone https://github.com/arturgoms/PyinArm.git
 ```
The next step is install all the dependencies( if you already have pip installed):
 ```bash
      pip install -U -r  requirements.txt
 ```
If dont ( Windows ):
 ```bash
      python get-pip.py
 ```
(macOs):

 ```bash
      sudo easy_install pip
 ```
 After enter the directory you can now execute PyinArm.py:
 ```bash
      sudo python PyinArm.py
 ```


## License
Usage is provided under the [MIT License](http://opensource.org/licenses/mit-license.php). See LICENSE for the full details.
